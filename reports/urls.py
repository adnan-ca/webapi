from django.urls import path
from rest_framework import urlpatterns
from .views import employeeList

app_name = 'reports'
urlpatterns = [
    path('', employeeList.as_view(), name='api-get')
]
