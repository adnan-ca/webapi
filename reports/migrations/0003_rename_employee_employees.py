# Generated by Django 3.2.4 on 2021-06-28 07:09

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reports', '0002_rename_employees_employee'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Employee',
            new_name='employees',
        ),
    ]
