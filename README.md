# README #


### What is this repository for? ###

* A Simple API to get list of all employees in JSON format using Django Rest Framework

### How do I get set up? ###

* To clone the repository to your local machine go to Desktop using Terminal and run
    + git clone https://adnan-ca@bitbucket.org/adnan-ca/webapi.git
* To install django using terminal Run
    + pip install django
* To install Django Rest Framework Run 
    + pip install djangorestframework
* Go to the cloned directory  using the terminal and Run
    + python manage.py makemigrations
    + python manage.py migrate
    + python manage.gy runserver
* To create a superuser Run
    + python manage.py createsuperuser


### How to use ###

* Go to localhost:8000/admin and enter superuser credentials
* Create multiple employees using administrator page
* Go to localhost:8000/employees to get list of all employees in JSON format



